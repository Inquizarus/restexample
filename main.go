package main

import (
	"log"
	"net/http"
	"os"

	"gitlab.com/inquizarus/rest"
)

func main() {
	tlsConfig := rest.ServeTLSConfig{
		Enabled:          false,
		Strict:           false,
		Preload:          false,
		StrictSubDomains: false,
		CertPath:         "restexample.crt",
		KeyPath:          "restexample.key",
	}

	logger := log.New(os.Stdout, "restexample: ", log.LstdFlags)

	config := rest.ServeConfig{
		Port:   "8080",
		TLS:    tlsConfig,
		Logger: logger,
		Middlewares: []rest.Middleware{
			rest.WithJSONContent(),
			rest.WithStrictTransportSecurity(tlsConfig),
		},
		Handlers: []rest.Handler{
			&rest.BaseHandler{
				Path: "/",
				Get: func(w http.ResponseWriter, r *http.Request, p map[string]string) {
					defer r.Body.Close()
					w.Write([]byte(`{"message": "Hello, World!"}`))
				},
			},
		},
	}
	rest.Serve(config)
}
